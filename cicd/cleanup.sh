#!/bin/sh

USER_NAME=`id -un`

DEV_PROJECT=${USER_NAME}-dev
CICD_PROJECT=${USER_NAME}-cicd

# delete project
oc delete project ${DEV_PROJECT}
oc delete project ${CICD_PROJECT}

